package main

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func TestMain(m *testing.M) {
	ensureEnvDeclared()
	rand.Seed(time.Now().UnixNano())
	connectToMongo(context.Background())
	code := m.Run()
	collection.Database().Client().Disconnect(context.Background())
	os.Exit(code)
}
func TestEmptyTable(t *testing.T) {
	err := clearTable()
	if err != nil {
		t.Error(err)
	} else if users, err := getUsersOverHTTP("", 0, 0); err != nil {
		t.Error(err)
	} else if len(users) != 0 {
		t.Errorf("Expected an empty array. Got %v", users)
	}
}

func TestGetNonExistentUser(t *testing.T) {
	if err := clearTable(); err != nil {
		t.Error(err)
	} else if user, err := getUserOverHTTP(primitive.NewObjectID().Hex()); !errors.Is(err, mongo.ErrNoDocuments) {
		t.Error(err)
	} else if user != nil {
		t.Errorf("expected to get 'nil' user. got %v", user)
	}
}
func TestCreateUser(t *testing.T) {
	name, password := randStringRunes(30), randStringRunes(25)
	u := &user{
		Name:     name,
		Password: password,
	}
	if err := clearTable(); err != nil {
		t.Error(err)
	} else if err := addUserOverHTTP(u); err != nil {
		t.Error(err)
	} else if u.Name != name || u.Password != password {
		t.Errorf("expected user to have user/password: %s/%s. got %s/%s (id:%s)", name, password, u.Name, u.Password, u.ID.Hex())
	}
}
func TestGetUsers(t *testing.T) {
	u := &user{
		Name:     randStringRunes(25),
		Password: randStringRunes(15),
	}
	if err := clearTable(); err != nil {
		t.Error(err)
	} else if err := addUserOverHTTP(u); err != nil {
		t.Error(err)
	} else if users, err := getUsersOverHTTP("", 0, 0); err != nil {
		t.Error(err)
	} else if *users[0] != *u {
		t.Errorf("expected %s. got %s", u, users[0])
	}
}

func TestUpdateUser(t *testing.T) {
	name, password := randStringRunes(50), randStringRunes(30)
	u := &user{
		Name:     randStringRunes(50),
		Password: randStringRunes(30),
	}
	if err := clearTable(); err != nil {
		t.Error(err)
	} else if err := addUserOverHTTP(u); err != nil {
		t.Error(err)
	} else if err := updateUserOverHTTP(u.ID.Hex(), &user{
		Name:     name,
		Password: password,
	}); err != nil {
		t.Error(err)
	} else if latest, err := getUserOverHTTP(u.ID.Hex()); err != nil {
		t.Error(err)
	} else if latest.Name != name || latest.Password != password {
		t.Errorf("expected user to have user/password: %s/%s. got %s/%s (id:%s)", name, password, u.Name, u.Password, u.ID.Hex())
	}
}

func TestDeleteUser(t *testing.T) {
	u := &user{
		Name:     randStringRunes(55),
		Password: randStringRunes(45),
	}
	if err := clearTable(); err != nil {
		t.Error(err)
	} else if err := addUserOverHTTP(u); err != nil {
		t.Error(err)
	} else if err := deleteUserOverHTTP(u.ID.Hex()); err != nil {
		t.Error(err)
	} else if _, err := getUserOverHTTP(u.ID.Hex()); !errors.Is(err, mongo.ErrNoDocuments) {
		t.Error(err)
	}
}

func clearTable() error {
	_, err := collection.DeleteMany(context.Background(), bson.M{})
	if err != nil {
		return fmt.Errorf("error clearing table. %s", err)
	}
	return nil
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func newRequest(method string, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, url, body)
	req.Header.Add("Content-Type", "application/json")
	return req, err
}

func getUsersOverHTTP(name string, limit, offset int) ([]*user, error) {
	req, err := newRequest(http.MethodGet, "/users", nil)
	if err != nil {
		return nil, err
	}
	if name != "" {
		req.URL.Query().Add("name", name)
	}
	if limit != 0 {
		req.Form.Add("count", strconv.Itoa(limit))
	}
	if offset != 0 {
		req.Form.Add("start", strconv.Itoa(offset))
	}
	resp, err := executeRequest(req, http.StatusOK)
	if err != nil {
		return nil, err
	}
	var users []*user
	return users, json.NewDecoder(resp.Body).Decode(&users)
}

func getUserOverHTTP(id string) (*user, error) {
	req, err := newRequest(http.MethodGet, fmt.Sprintf("/user/%s", id), nil)
	if err != nil {
		return nil, err
	}
	resp, err := executeRequest(req, http.StatusOK)
	if err != nil {
		return nil, err
	}
	return unmarshalUserOrError(resp.Body)
}

func updateUserOverHTTP(id string, u *user) error {
	bodyData, err := json.Marshal(u)
	if err != nil {
		return err
	}
	req, err := newRequest(http.MethodPut, fmt.Sprintf("/user/%s", id), bytes.NewBuffer(bodyData))
	if err != nil {
		return err
	}
	defer req.Body.Close()
	resp, err := executeRequest(req, http.StatusOK)
	if err != nil {
		return err
	}
	newUser, err := unmarshalUserOrError(resp.Body)
	if err != nil {
		return err
	}
	*u = *newUser
	return nil
}
func addUserOverHTTP(u *user) error {
	bodyData, err := json.Marshal(u)
	if err != nil {
		return err
	}
	req, err := newRequest(http.MethodPost, "/user", bytes.NewBuffer(bodyData))
	if err != nil {
		return err
	}
	defer req.Body.Close()
	resp, err := executeRequest(req, http.StatusOK)
	if err != nil {
		return err
	}
	insertedUser, err := unmarshalUserOrError(resp.Body)
	if err != nil {
		return err
	}
	*u = *insertedUser
	return nil
}
func deleteUserOverHTTP(id string) error {
	req, err := newRequest(http.MethodDelete, fmt.Sprintf("/user/%s", id), nil)
	if err != nil {
		return err
	}
	_, err = executeRequest(req, http.StatusOK)
	return err
}
func executeRequest(req *http.Request, expectedStatusCode int) (*httptest.ResponseRecorder, error) {
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	if expectedStatusCode != resp.Code {
		err := fmt.Errorf("expected response code %d. got %d", expectedStatusCode, resp.Code)
		if resp.Body != nil {
			var sb strings.Builder
			io.Copy(&sb, resp.Body)
			return nil, fmt.Errorf("%w. %s", err, sb.String())
		}
		return nil, err
	}
	return resp, nil
}

func unmarshalUserOrError(body io.Reader) (*user, error) {
	data, err := ioutil.ReadAll(body)
	var u user
	d := json.NewDecoder(bytes.NewBuffer(data))
	d.DisallowUnknownFields()
	err = d.Decode(&u)
	if err != nil {
		var m map[string]string
		err = json.Unmarshal(data, &m)
		if err != nil {
			return nil, err
		}
		switch message := m["message"]; message {
		case "":
			return nil, fmt.Errorf("%v", m)
		case mongo.ErrNoDocuments.Error():
			return nil, mongo.ErrNoDocuments
		default:
			return nil, errors.New(message)
		}
	}
	return &u, nil
}
