# Release 1.0.0 - 24 dec 2020
- Added iac
- Added docker compose
- Finished everything up lol

# Release 0.0.3 - 13 dec 2020 
- Added unit tests 
- Added automatic testing and building through gitlab pipelines
- Connection URI is now built by the app at runtime
- Connection params (table name, authentification etc...) are declared as env variables

# Release 0.0.2 - 21 sept 2020 
- Initialized gitlab CI/CD
- Initialized Dockerfile

# Release 0.0.1 - 17 sept 2020 
Basic implementation of app
- CRUD server
- Connect to MongoDB through connection uri provided in cmd line 
