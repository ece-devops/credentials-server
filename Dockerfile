FROM alpine:latest

WORKDIR /usr/local/bin

COPY credentials-server .
ENTRYPOINT ["./credentials-server"]