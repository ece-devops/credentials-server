package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	envConnectionURI  = "CS_CONNECTION_URI"
	envDatabaseName   = "CS_DATABASE_NAME"
	envCollectionName = "CS_COLLECTION_NAME"
)

var (
	connectionURI  string
	collectionName string
	databaseName   string
)

var (
	cout = log.New(os.Stdout, "", 0)
	cerr = log.New(os.Stderr, "[ERROR] ", log.Lshortfile)
	clog = log.New(os.Stdout, "[LOG] ", log.LstdFlags)
)

var (
	collection *mongo.Collection
	router     *mux.Router
)

func ensureEnvDeclared() {
	getEnvOrFail := func(envVarName string) string {
		envVarValue := os.Getenv(envVarName)
		if envVarValue == "" {
			cerr.Fatalln(envVarName, "not set")
		}
		return envVarValue
	}
	databaseName = getEnvOrFail(envDatabaseName)
	collectionName = getEnvOrFail(envCollectionName)
	connectionURI = getEnvOrFail(envConnectionURI)
}

func init() {
	router = mux.NewRouter()
	router.HandleFunc("/users", getUsers).Methods(http.MethodGet)
	router.HandleFunc("/users", getUsers).Methods(http.MethodGet).Queries("name", "{name}")
	router.HandleFunc("/user/{id}", getUser).Methods(http.MethodGet)
	router.HandleFunc("/user", addUser).Methods(http.MethodPost)
	router.HandleFunc("/user/{id}", updateUser).Methods(http.MethodPut)
	router.HandleFunc("/user/{id}", deleteUser).Methods(http.MethodDelete)
}

type user struct {
	ID       primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Name     string             `json:"name,omitempty" bson:"name,omitempty"`
	Password string             `json:"password,omitempty" bson:"password,omitempty"`
}

func (u user) String() string {
	var sb strings.Builder
	sb.WriteRune('[')
	if !u.ID.IsZero() {
		sb.WriteString(fmt.Sprintf("ID='%s', ", u.ID.Hex()))
	}
	sb.WriteString(fmt.Sprintf("Name='%s', Password='%s']", u.Name, u.Password))
	return sb.String()
}

// Some versioning variables
var (
	Name    string
	Build   string
	Version string
)

func main() {
	fs := flag.NewFlagSet(Name, flag.ExitOnError)
	verbose := fs.Bool("verbose", false, "display what's happening")
	version := fs.Bool("version", false, "show the version of this command")
	fs.Parse(os.Args[1:])

	if !*verbose {
		clog.SetOutput(ioutil.Discard)
	}
	if *version {
		cout.Printf("%s - %s - %s", Name, Version, Build)
		return
	}

	ensureEnvDeclared()
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	connectToMongo(ctx)
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	server := &http.Server{
		Addr:         fmt.Sprint(":", port),
		Handler:      router,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
	}
	go func() {
		cout.Println(server.ListenAndServe())
	}()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	err := server.Shutdown(context.Background())
	if err != nil {
		cerr.Fatalln(err)
	}
	err = collection.Database().Client().Disconnect(context.Background())
	if err != nil {
		log.Fatalln(err)
	}
}

func connectToMongo(ctx context.Context) {
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connectionURI))
	if err != nil {
		cerr.Fatalln("could not connect to mongo.", err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		cerr.Fatalln(err)
	}
	database := client.Database(databaseName)
	if database == nil {
		cerr.Fatalln("could not get handle for database ", databaseName)
	}

	collection = database.Collection(collectionName)
	if collection == nil {
		cerr.Fatalln("could not get handle for collection ", collectionName)
	}
	clog.Printf("Using Database/Collection: %s/%s", databaseName, collectionName)
}

func updateUser(w http.ResponseWriter, r *http.Request) {
	if r.Body != nil {
		defer r.Body.Close()
	}
	id, err := primitive.ObjectIDFromHex(mux.Vars(r)["id"])
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	var u user
	err = json.NewDecoder(r.Body).Decode(&u)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	cout.Printf("setting user with id %v to %v", id, u)
	_, err = collection.UpdateOne(context.Background(), bson.M{"_id": id}, bson.M{"$set": u})
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	if u.ID.IsZero() {
		u.ID = id
	}
	respondWithJSON(w, http.StatusOK, &u)
}

func deleteUser(w http.ResponseWriter, r *http.Request) {
	if r.Body != nil {
		defer r.Body.Close()
	}
	id, err := primitive.ObjectIDFromHex(mux.Vars(r)["id"])
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	cout.Println("deleting user", id.String())
	_, err = collection.DeleteOne(context.Background(), bson.M{"_id": id})
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJSON(w, http.StatusOK, make(map[string]string))
}

func addUser(w http.ResponseWriter, r *http.Request) {
	if r.Body != nil {
		defer r.Body.Close()
	}
	var u user
	err := json.NewDecoder(r.Body).Decode(&u)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	cout.Println("inserting user", u.String())
	res, err := collection.InsertOne(context.Background(), &u)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	if id, ok := res.InsertedID.(primitive.ObjectID); ok {
		u.ID = id
	}
	respondWithJSON(w, http.StatusOK, &u)
}

func getUser(w http.ResponseWriter, r *http.Request) {
	if r.Body != nil {
		defer r.Body.Close()
	}
	id, err := primitive.ObjectIDFromHex(mux.Vars(r)["id"])
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	var u user
	err = collection.FindOne(context.Background(), bson.M{"_id": id}).Decode(&u)
	switch err {
	case mongo.ErrNoDocuments:
		respondWithError(w, http.StatusOK, mongo.ErrNoDocuments.Error())
	case nil:
		respondWithJSON(w, http.StatusOK, &u)
	default:
		respondWithError(w, http.StatusInternalServerError, err.Error())
	}
}

func getUsers(w http.ResponseWriter, r *http.Request) {
	if r.Body != nil {
		defer r.Body.Close()
	}
	var sb strings.Builder
	var opts []*options.FindOptions
	if count := r.FormValue("count"); count != "" {
		v, err := strconv.ParseInt(count, 10, 64)
		if err != nil {
			respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}
		opts = append(opts, options.Find().SetLimit(v))
		sb.WriteString(fmt.Sprintf("limit=%d, ", v))
	}
	if start := r.FormValue("start"); start != "" {
		v, err := strconv.ParseInt(start, 10, 64)
		if err != nil {
			respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}
		opts = append(opts, options.Find().SetSkip(v))
		sb.WriteString(fmt.Sprintf("skip=%d", v))
	}

	var filter bson.M
	if name := r.URL.Query().Get("name"); name != "" {
		filter = bson.M{"name": name}
	}

	cout.Println("getting rows. filter -", filter, ";", "options -", sb.String())
	cursor, err := collection.Find(context.Background(), filter, opts...)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	var results []user = make([]user, 0)
	err = cursor.All(context.Background(), &results)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJSON(w, http.StatusOK, results)
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"message": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
