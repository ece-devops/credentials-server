This is a web server that serves user credentials from a MongoDB database 

**There is currently a running deployment accessible at https://creds-srv-prod.herokuapp.com/**. See below for all available endpoints

**Access to relevant 3rd party services has been granted to sergei@adaltas.com**

# Tasks list
- [X] Enriched web application with automated tests
    - [X] Web application 
    - [X] Automated tests
- [X] Continuous Integration and Continuous Delivery (and Deployment)
    - [X] Continuous Integration 
    - [X] Continuous Delivery
    - [X] Continuous Deployment: Used Heroku + [MongoDB Atlas](https://cloud.mongodb.com/v2/5f5b3470c7a9145d124aefe7#clusters) as production. VM provisioning via Ansible for iac
- [X] Infrastructure as code using Ansible
- [X] Containerisation with Docker
- [X] Orchestration with Docker Compose
- [ ] Orchestration with Kubernetes
- [ ] Service mesh using Istio
- [X] Accurate project documentation in README.md file
- [X] **Bonus** : Use different tools and platforms instead of what has been passed in the labs, for example: GitLab CI/CD, Netlify, etc -> *used GitLab CI/CD*
- [X] **Bonus** : Use different language (Java, Ruby, Python etc.) to develop the application -> *used Go*
- [X] **Bonus** : Use another database (like MongoDB, MySQL, ...) -> *used MongoDB* 
- [X] **Bonus** : Mirror repo to GitHub and use integrations with GitHub -> *used travis and heroku [link to mirror repo](https://github.com/jbztt/Credentials-Server-Mirror)*


# CI/CD Workflow
- Push to GitLab:
    - Run unit tests
    - Build sources and generate executables
    - Build and pulish Docker image to [GitLab registry](https://gitlab.com/ece-devops/credentials-server/container_registry)
    - Configure and Provision a virtual environment and run the application using IaC approach
        - Installation using Vagrant and Ansible Provisioner
            - Prepare a virtual environment (Vagrantfile that defines the VM to be managed by Vagrant (1 Ubuntu 1804 VM named virtual_server using the VirtualBox provisioner) / Ansible playbooks)
            - Install required system packages & prerequisites
            - Install Docker 
            - Run the application in docker containers using tagged image from the gitlab registry (Start MongoDB/      Server containers)
        - Create and provision a virtual machine (VM)
            - Vagrant commands (vagrant up / provision with ansible)
        - Test the installation locally (http://20.20.20.2:8080/users)
    - Mirror to GitHub:
        - Run Unit tests through travis
        - (Attempted to use circle-ci and Netlify integrations)
        - Forward to heroku
            - Build from sources
            - Run app with a MongoDB provisionned at mongo Atlas

# Quickstart
## With Docker
- Create a network: `docker network create net0`
- Start a MongoDB container: `docker run -d --network net0 --name mongodb mongo:latest`
- Start the app: `docker run -d -e CS_CONNECTION_URI=mongodb://mongodb:27017 -e CS_COLLECTION_NAME=users -e CS_DATABASE_NAME=credentials --network net0 -p 8080:8080 registry.gitlab.com/ece-devops/credentials-server/master:latest -verbose`
## With Docker Compose
- clone the repo: `git clone https://gitlab.com/ece-devops/credentials-server.git`
- `cd credentials-server`
- `docker-compose up --build`
## From sources (supposing that a MongoDB instance is already running)
- Install the latest and greatest [golang compiler](https://golang.org/doc/install)
- Build the app: go build
- Declare environment variables
    - `export CS_CONNECTION_URI=<connection-URI>` : to connect to database (https://docs.mongodb.com/manual/reference/connection-string/)
    - `export CS_DATABASE_NAME=<db-name>` : database to use to access data
    - `export CS_COLLECTION_NAME=<collection-name>` : table to use to access data
    - `export PORT=<port-number>` : port on which to expose the app
- Run the app : `./credentials-server -verbose`

# Overview
## Data types
The app currently only returns JSON-encoded bodies of the following data-types:
- `user{id:string, name:string, password:string}` to interact with the database and clients
- `error{message:string}` to report errors to clients
## CRUD
### Create
- End-point: `/user`
- Request:  
    - Method: POST
    - Body: a JSON-encoded `user` object without an id. id will be generated automatically
- Response:
    - Code: 
        - On success: 200 
        - On error: 500
    - Body: 
        - On success: a JSON-encoded `user` object representing the user that was just created
        - On error: a JSON-encoded `error` object
- Sample with CURL on a server running on localhost:8080: 
    - `curl -X POST -d '{"name":"myName", "password":"myPassword"}' localhost:8080/user`
    - `{"id":"5fe3992c15a9d21eaa83191c","name":"myName","password":"myPassword"}`
### Update
- End-point: `/user/<id>`
- Request:
    - Method: PUT
    - Body: A JSON-encoded `user` object with the values that need to be set
- Response:
    - Code:
        - On success: 200
        - On error: 500
    - Body:
        - On success: A JSON-encoded `user` object with the updated fields
        - On error: A JSON-encoded `error` object
- Sample with CURL on a server running on localhost:8080:
    - `curl -X PUT -d '{"name":"myFirstName_myLastName", "password":"mySuperLongPassword"}' localhost:8080/user/5fe3992c15a9d21eaa83191c`
    - `{"id":"5fe3992c15a9d21eaa83191c","name":"myFirstName_myLastName","password":"mySuperLongPassword"}`

### Read one
- End-point: `/user/<id>`
- Request:
    - Method: GET
    - Body: No body is expected
- Response:
    - Code:
        - On success or **if user not found**: 200
        - On error: 500
    - Body:
        - On success: a JSON encoded `user` object
        - On error or **if user not found** : a JSON encoded `error` object
- Sample with CURL on a server running on localhost:8080:
    - `curl -X GET http://localhost:8080/user/5fe3992c15a9d21eaa83191c`
    - `{"id":"5fe3992c15a9d21eaa83191c","name":"myFirstName_myLastName","password":"mySuperLongPassword"}`

### Read Many
- End-points: /users for all users or `/users?name=<name>` to filter by name
- Request:
    - Method: GET
    - Form: 
        - count: int -> limit the number of results returned
        - start: int -> set offset when scanning 
    - Body: No body is expected
- Response:
    - Code: 
        - On success: 200
        - On error: 500
    - Body:
        - On success: a JSON-encoded array of `user` objects
        - On error: a JSON-encoded `error` object
- Sample with CURL on a server running on localhost:8080:
    - `curl -X GET -F count=2 -F start=3 http://localhost:8080/users?name=toto`
    - `[{"id":"5fe3aa1615a9d21eaa83191e","name":"toto","password":"phoneNumber"},{"id":"5fe3aa1e15a9d21eaa83191f","name":"toto","password":"123456"}]`

### Delete
- End-point: `/user/<id>`
- Request:
    - Method: DELETE
    - Body: No body is expected
- Response: 
    - Code: 
        - On success: 200
        - On error: 500
    - Response:
        - On success: an empty JSON object
        - On error: a JSON-encoded `error` object 
- Sample with CURL on a server running on localhost:8080
    - `curl -X DELETE http://localhost:8080/user/5fe3aa1615a9d21eaa83191e`

